//
// Created by Eduard Luhtonen on 26.12.19.
//
#include "Vector.h"

Vector::Vector(int s) :elem{new double[s]}, sz{s} {}

double& Vector::operator[](int i) {
    return elem[i];
}

int Vector::size() {
    return sz;
}
