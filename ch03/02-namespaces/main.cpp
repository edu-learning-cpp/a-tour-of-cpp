#include <iostream>

namespace My_code {
    class complex {
    private:
        int x;
        int y;
    public:
        complex(int x, int y) {
            this->x = x;
            this->y = y;
        }
        int real() { return x; }
        int imag() { return y; }
    };
    complex sqrt(complex);
    int main();
}

My_code::complex My_code::sqrt(class My_code::complex z) {
    return z;
}

int My_code::main() {
    complex z {1,2};
    auto z2 = sqrt(z);
    std::cout << '{' << z2.real() << ',' << z2.imag() << '}' << std::endl;
    return 0;
}

int main() {
    return My_code::main();
}
