//
// Created by Eduard Luhtonen on 29.12.19.
//
#ifndef INC_01_MODULARITY_VECTOR_H
#define INC_01_MODULARITY_VECTOR_H

class Vector {
public:
    Vector(int s);
    double& operator[](int i);
    int size();
private:
    double* elem;
    int sz;
};

#endif //INC_01_MODULARITY_VECTOR_H
