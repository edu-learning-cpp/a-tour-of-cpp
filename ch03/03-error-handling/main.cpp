#include <iostream>
#include "Vector.h"

using namespace std;

int main() {
    try {
        Vector v(-27);
    } catch (length_error& err) {
        // handle negative size
        cerr << err.what() << '\n';
    } catch (std::bad_alloc& err) {
        // handle memory exhaustion
        cerr << err.what() << '\n';
    }
    Vector v(3);

    try {
        v[v.size()] = 7;
    } catch (out_of_range& err) {
        cerr << err.what() << '\n';
    }

    return 0;
}
