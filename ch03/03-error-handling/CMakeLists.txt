cmake_minimum_required(VERSION 3.15)
project(03_error_handling)

set(CMAKE_CXX_STANDARD 17)

add_executable(03_error_handling main.cpp Vector.h Vector.cpp)