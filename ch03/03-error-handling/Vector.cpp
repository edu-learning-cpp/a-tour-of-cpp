//
// Created by Eduard Luhtonen on 29.12.19.
//
#include <iostream>
#include "Vector.h"

using namespace std;

Vector::Vector(int s) {
    if (s < 0) {
        throw length_error{"Vector constructor: negative size"};
    }
    elem = new double[s];
    sz = s;
}

double& Vector::operator[](int i) {
    if (i < 0 || size() <= i) {
        throw out_of_range{"Vector::operator[]"};
    }
    return elem[i];
}

int Vector::size() {
    return sz;
}
