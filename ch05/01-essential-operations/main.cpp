#include <iostream>
#include <thread>
#include <vector>

using namespace std;

class Sometype {};

class X {
public:
    X(Sometype);            // "ordinary constructor": create an object
    X() {}                  // default constructor
    X(const X& x) {}        // copy constructor
    X(X&&);                 // move constructor
    X& operator=(const X&); // copy assignment: clean up target and copy
    X& operator=(X&&);      // move assignment: clean up target and move
    ~X() {}                 // destructor: clean up

    // comparisons
    bool operator==(const X&) { return true; }
    bool operator!=(const X& x) { return !(*this == x); }
    // ...
};

void useX(Sometype value) {
    X make(Sometype);
    //X x = make(value);
}

// explicit about generating default implementations
class Y {
public:
    Y(Sometype);
    Y(const Y&) = default;   // I really do want the default copy constructor
    Y(Y&&) = default;        // and the default move constructor
    // ...
};

class Vector {
public:
    explicit Vector() = default;
    explicit Vector(int s) :elem{new double[s]}, sz{s} {}    // no implicit conversion from int to Vector
    ~Vector() { delete[] elem; }           // destructor: release resources

    Vector(const Vector& a);               // copy constructor
    Vector& operator=(const Vector& a);    // copy assignment

    Vector(Vector&& a);                    // move constructor
    Vector& operator=(Vector&& a);         // move assignment

    double& operator[](int i);
    const double& operator[](int i) const { return elem[i]; }

    int size() const { return sz; }

    void swap(Vector& other) {
        using std::swap;
        swap(sz, other.sz);
        swap(elem, other.elem);
    }
private:
    double* elem;
    int sz;
};

// none of essential operations
struct Z {
    Vector v;
    string s;
};

void useZ() {
    Z z1;          // default initialize z1.v and z1.s
    Z z2 = z1;     // default copy z1.v and z1.s
}

class Shape {
public:
    Shape(const Shape&) =delete;            // no copy operations
    Shape& operator=(const Shape&) =delete;
    // ...
};

/*
void copy(Shape& s1, const Shape& s2)
{
    s1 = s2;  // error: Shape copy is deleted
}
*/

// conversions
void useVector() {
    Vector v1(7);   // OK: v1 has 7 elements
    // Vector v2 = 7;  // error: no implicit conversion from int to Vector
}

// Member Initializers
class complex {
    double re = 0;
    double im = 0; // representation: two doubles with default value 0.0
public:
    complex(double r, double i) :re{r}, im{i} {}    // construct complex from two scalars: {r,i}
    complex(double r) :re{r} {}                     // construct complex from one scalar: {r,0}
    complex() {}                                    // default complex: {0,0}
    // ...
};

// Copy and Move
double& Vector::operator[](int i) {
    return elem[i];
}

void test(complex z1)
{
    complex z2 {z1};    // copy initialization
    complex z3;
    z3 = z2;            // copy assignment
    // ...
}

// Copying Containers
void bad_copy(Vector v1)
{
    Vector v2 = v1;    // copy v1's representation into v2
    v1[0] = 2;         // v2[0] is now also 2!
    v2[1] = 3;         // v1[1] is now also 3!
}

// copy constructor
Vector::Vector(const Vector& a)   // copy constructor
        :elem{new double[a.sz]},     // allocate space for elements
         sz{a.sz}
{
    for (int i=0; i!=sz; ++i)    // copy elements
        elem[i] = a.elem[i];
}

// copy assignment
Vector& Vector::operator=(const Vector& a)     // copy assignment
{
    double* p = new double[a.sz];
    for (int i=0; i!=a.sz; ++i)
        p[i] = a.elem[i];
    delete[] elem;         // delete old elements
    elem = p;
    sz = a.sz;
    return *this;
}

// move constructor
Vector::Vector(Vector &&a)
        : elem{a.elem},          // "grab the elements" from a
          sz{a.sz} {
    a.elem = nullptr;       // now a has no elements
    a.sz = 0;
}
// move assignment
Vector& Vector::operator=(Vector &&a) {
    a.swap(*this);
    return *this;
}

Vector operator+(const Vector &a, const Vector &b) {
    if (a.size() != b.size())
        throw length_error{"Vector size mismatch"};

    Vector res(a.size());
    for (int i = 0; i != a.size(); ++i)
        res[i] = a[i] + b[i];
    return res;
}

void f(const Vector &x, const Vector &y, const Vector &z) {
    Vector r;
    // ...
    // with only copy constructor next line will copy vector at least twice
    // but this move constructor and assignment, compiler will choose them and where will be no copy
    r = x + y + z;
    // ...
}
Vector h() {
    Vector x(1000);
    Vector y(2000);
    Vector z(3000);
    z = x;              // we get a copy (x might be used later in f())
    y = std::move(x);   // we get a move (move assignment)
    // ... better not use x here ...
    return z;           // we get a move
}

// RESOURCE MANAGEMENT
void heartbeat() {
    // do nothing
}

std::vector<thread> my_threads;

Vector init(int n) {
    thread t{heartbeat};                 // run heartbeat concurrently (in a separate thread)
    my_threads.push_back(std::move(t));   // move t into my_threads (§13.2.2)
    // ... more initialization ...

    Vector vec(n);
    for (int i = 0; i != vec.size(); ++i)
        vec[i] = 777;
    return vec;                      // move vec out of init()
}

void foo() {
    auto v = init(1'000'000);
}

// CONVENTIONAL OPERATIONS
void comparisons() {
    X a = X();
    X b = a;
    assert(a == b); // if a!=b here, something is very odd
}

int main() {
    cout << "Essential operations in C++" << endl;
    return 0;
}
