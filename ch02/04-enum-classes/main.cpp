#include <iostream>

using namespace std;

enum Colour {
    red, blue, green
};
enum class TrafficLight {
    green, yellow, red
};

TrafficLight &operator++(TrafficLight &t) {
    switch (t) {
        case TrafficLight::green:
            return t = TrafficLight::yellow;
        case TrafficLight::yellow:
            return t = TrafficLight::red;
        case TrafficLight::red:
            return t = TrafficLight::green;
    }
}

ostream& operator<<(ostream& out, TrafficLight t) {
    switch (t) {
        case TrafficLight::red: out << "red"; break;
        case TrafficLight::yellow: out << "yellow"; break;
        case TrafficLight::green: out << "green"; break;
    }
    return out;
}

ostream& operator<<(ostream& out, Colour c) {
    switch (c) {
        case Colour ::red: out << "red"; break;
        case Colour::blue: out << "blue"; break;
        case Colour ::green: out << "green"; break;
    }
    return out;
}

int main() {
    Colour c = Colour::red;
    cout << "Colour: " << c << endl;
    int col = Colour::green;
    cout << "Colour: " << col << endl;

    TrafficLight t = TrafficLight::red;
    cout << "Traffic light: " << t << endl;
    TrafficLight next = ++t;
    cout << "Next traffic ligght: " << next << endl;

    return 0;
}
