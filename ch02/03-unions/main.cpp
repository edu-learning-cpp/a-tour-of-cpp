#include <iostream>
#include <variant>

using namespace std;

enum Type {
    ptr, num
};

class Node;

struct Entry {
    string name;
    Type t;
    Node *p;
    int i;
};

void f(Entry *pe) {
    cout << pe->name << " ";
    if (pe->t == num) {
        cout << pe->i << endl;
    }
    // ...
}

void use_entry(string name, Type t) {
    Entry e;
    e.name = name;
    e.t = t;
    if (e.t == num) {
        e.i = 42;
    }

    f(&e);
}

union Value {
    int i;
    Node *p;
};

struct UEntry {
    string name;
    Type t;
    Value v;
};

void fu(UEntry *pe) {
    cout << pe->name << " ";
    if (pe->t == num) {
        cout << pe->v.i << endl;
    }
    // ...
}

void use_uentry(string name, Type t) {
    UEntry e;
    e.name = name;
    e.t = t;
    if (e.t == num) {
        e.v.i = 42;
    }

    fu(&e);
}

struct EntryVariant {
    string name;
    variant<Node *, int> v;
};

void fv(EntryVariant *pe) {
    cout << pe->name << " ";
    if (holds_alternative<int>(pe->v))  // does *pe hold an int? (see §13.5.1)
        cout << get<int>(pe->v);      // get the int
    // ...
    cout << endl;
}

void use_ventry(string name) {
    EntryVariant e;
    e.name = name;
    e.v = 42;

    fv(&e);
}


int main() {
    use_entry("number 1", num);
    use_uentry("number 2", num);
    use_ventry("number 3");

    return 0;
}
