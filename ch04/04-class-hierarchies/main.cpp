#include <iostream>
#include <memory>
#include <vector>

using namespace std;

class Point {
public:
    Point(double x, double y) :x_coord{x}, y_coord{y} {}

private:
    double x_coord, y_coord; // coordinates of the point
};

class Shape {
public:
    virtual Point center() const = 0;     // pure virtual
    virtual void move(Point to) = 0;

    virtual void draw() const = 0;       // draw on current "Canvas"
    virtual void rotate(int angle) = 0;

    virtual ~Shape() {}                    // destructor
    // ...
};

void rotate_all(vector<unique_ptr<Shape>>& v, int angle) {
    for (auto& p : v) {
        p->rotate(angle);
    }
}

class Circle : public Shape {
public:
    Circle(Point p, int rad) : x{p}, r{rad} {}       // constructor

    Point center() const override
    {
        return x;
    }
    void move(Point to) override
    {
        x = to;
    }

    void draw() const override {}
    void rotate(int) override {}         // nice simple algorithm
private:
    Point x;   // center
    int r;     // radius
};

class Smiley : public Circle {  // use the circle as the base for a face
public:
    Smiley(Point p, int rad) : Circle{p, rad}, mouth{nullptr} { }

    ~Smiley()
    {
        delete mouth;
        for (auto p : eyes)
            delete p;
    }

    void move(Point to) override {}

    void draw() const override;
    void rotate(int) override {}

    void add_eye(Shape* s)
    {
        eyes.push_back(s);
    }
    void set_mouth(Shape* s) {}
    virtual void wink(int i) {}     // wink eye number i

    // ...

private:
    vector<Shape*> eyes;          // usually two eyes
    Shape* mouth;
};

void Smiley::draw() const
{
    Circle::draw();
    for (auto p : eyes)
        p->draw();
    mouth->draw();
}

class Triangle : public Shape {
public:
    Triangle(Point po1, Point po2, Point po3)       // constructor
        : p1{po1}, p2{po2}, p3{po3} {}

    Point center() const override
    {
        return p1;
    }
    void move(Point to) override
    {
        p1 = to;
    }

    void draw() const override {}
    void rotate(int) override {}         // nice simple algorithm
private:
    Point p1;
    Point p2;
    Point p3;
};

enum class Kind { circle, triangle, smiley };

unique_ptr<Shape> read_shape(istream& is) {
    // ... read shape header from is and find its Kind k ...
    Point p{1,2};
    Point p1{1,2};
    Point p2{1,2};
    Point p3{1,2};
    int r{3};
    Kind k = Kind::smiley;
    switch (k) {
        case Kind::circle:
            // read circle data {Point,int} into p and r
            return unique_ptr<Shape>{new Circle{p,r}};
        case Kind::triangle:
            // read triangle data {Point,Point,Point} into p1, p2, and p3
            return unique_ptr<Shape>{new Triangle{p1,p2,p3}};
        case Kind::smiley:
            // read smiley data {Point,int,Shape,Shape,Shape} into p, r, e1, e2, and m
            auto* e1 = new Circle{Point{4, 5}, 1};
            auto* e2 = new Circle{Point{6, 7}, 2};
            auto* m = new Circle{Point{8, 9}, 3};
            unique_ptr<Smiley> ps = make_unique<Smiley>(p,r);
            ps->add_eye(e1);
            ps->add_eye(e2);
            ps->set_mouth(m);
            return ps;
    }
}

void user()
{
    std::vector<unique_ptr<Shape>>v;
    while (cin)
        v.push_back(read_shape(cin));
    // draw_all(v);                            // call draw() for each element
    rotate_all(v,45);             // call rotate(45) for each element
    for (auto& ps : v) {
        // dynamic_cast to a pointer type returns nullptr if failed
        if (auto* p = dynamic_cast<Smiley*>(ps.get())) { // ... does ps point to a Smiley? ...
            p->wink(5);
        } else {
            // ... not a Smiley, try something else ...
        }
        // dynamic_cast to a reference type throws a bad_cast exception
        Smiley& r {dynamic_cast<Smiley&>(*ps)};   // somewhere, catch std::bad_cast
    }
    // with unique_ptr no need to explicitly call delete function
//    for (auto p : v)                        // remember to delete elements
//        delete p;
}

void user2(int x) {
    Shape *p = new Circle{Point{0, 0}, 10};
    // ...
    if (x < 0) throw out_of_range{"negative"};   // potential leak
    if (x == 0) return;         // potential leak
    // ...
    delete p;
}

int main() {
    std::cout << "Class hierarchy example" << std::endl;
    return 0;
}
