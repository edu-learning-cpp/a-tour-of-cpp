#include <iostream>
#include <list>

using namespace std;

class Vector {
public:
    Vector(int s) :elem{new double[s]}, sz{s} {
        for (int i = 0; i != s; ++i) {
            elem[i] = 0;
        }
    }
    Vector(initializer_list<double>);

    ~Vector() { delete[] elem; }

    double& operator[](int i) { return elem[i]; }
    int size() const { return sz; }

private:
    double* elem;
    int sz;
};

class Container {
public:
    virtual double& operator[](int) = 0;    // pure virtual function
    virtual int size() const = 0;           // const member function
    virtual ~Container() {}                 // destructor
};

void use(Container& c) {
    const int sz = c.size();

    for (int i = 0; i != sz; ++i) {
        cout << c[i] << " ";
    }
    cout << endl;
}

class Vector_container : public Container { // Vector_container implements Container
public:
    Vector_container(int s) : v(s) { }   // Vector of s elements
    ~Vector_container() {}

    double& operator[](int i) override { return v[i]; }
    int size() const override { return v.size(); }
private:
    Vector v;
};

void g() {
    Vector_container vc(10);
    use(vc);
}

class List_container : public Container {   // List_container implements Container
public:
    List_container() { }      // empty List
    List_container(initializer_list<double> il) : ld{il} { }
    ~List_container() {}
    double& operator[](int i) override;
    int size() const override { return ld.size(); }
private:
    list<double> ld;     // (standard-library) list of doubles
};

double& List_container::operator[](int i){
    for (auto &x : ld) {
        if (i == 0)
            return x;
        --i;
    }
    throw out_of_range{"List container"};
}

void h() {
    List_container lc = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    use(lc);
}

int main() {
    // Container c;                                // error: there can be no objects of an abstract class
    Container* p = new Vector_container(10);    // OK: Container is an interface

    cout << "=== Vector container" << endl;
    g();

    cout << "=== List container" << endl;
    h();
    return 0;
}
