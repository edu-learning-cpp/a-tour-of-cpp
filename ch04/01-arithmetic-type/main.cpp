#include <iostream>

using namespace std;

class complex {
    double re, im; // representation: two doubles
public:
    complex(double r, double i) :re{r}, im{i} {}    // construct complex from two scalars
    complex(double r) :re{r}, im{0} {}              // construct complex from one scalar
    complex() :re{0}, im{0} {}                      // default complex: {0, 0}

    double real() const { return re; }
    void real(double d) { re = d; }
    double imag() const { return im; }
    void imag(double d) { im = d; }

    complex& operator+=(complex z) {
        re += z.re;         // add to re and im
        im += z.im;
        return *this;       // and return the result
    }

    complex& operator-=(complex z) {
        re -= z.re;
        im -= z.im;
        return *this;
    }

    complex& operator*=(complex z) {
        double oldRe = re;
        re = re * z.re + im * (-1 * z.im);
        im = oldRe * z.im + im * z.re;
        return *this;
    }
    complex& operator/=(complex z) {
        double oldRe = re;
        double divisor =  (z.re * z.re + z.im * z.im);
        re = (re * z.re + im * z.im) / divisor;
        im = (im * z.re - oldRe * z.im) / divisor;
        return *this;
    }
};

complex operator+(complex a, complex b) { return a+=b; }
complex operator-(complex a, complex b) { return a-=b; }
complex operator-(complex a) { return {-a.real(), -a.imag()}; }    // unary minus
complex operator*(complex a, complex b) { return a*=b; }
complex operator/(complex a, complex b) { return a/=b; }
bool operator==(complex a, complex b)     // equal
{
    return a.real() == b.real() && a.imag() == b.imag();
}

bool operator!=(complex a, complex b)     // not equal
{
    return !(a == b);
}
string to_string(complex z) { return "(" + to_string(z.real()) + ", " + to_string(z.imag()) + ")"; }

void f(complex z) {
    complex a {2.3};
    complex b { 1/a };
    complex c { a + z * complex{1, 2.3} };
    if (c != b) c = -(b / a) + 2 * b;
    cout << "a = {" << a.real() << ", " << a.imag() << "}" << endl;
    cout << "b = {" << b.real() << ", " << b.imag() << "}" << endl;
    cout << "c = {" << c.real() << ", " << c.imag() << "}" << endl;
}

int main() {
    complex z = {1, 0};
    const complex cz {1, 3};
    cout << "z.real = " << z.real() << ", z.imag = " << z.imag() << endl;
    z = cz;
    cout << "z.real = " << z.real() << ", z.imag = " << z.imag() << endl;
    // cz = z; // error: complex::operator=() is a non-const member function

    f(z);

    complex a{2, 0};
    complex b{0, 1};
    cout << to_string(b) << " * " << to_string(b) << " = " << to_string(b * b) << endl;
    cout << to_string(a) << " / " << to_string(b) << " = " << to_string(a / b) << endl;
    cout << to_string(1) << " / " << to_string(b) << " = " << to_string(1 / b) << endl;

    return 0;
}
