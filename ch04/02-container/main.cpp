#include <iostream>

using namespace std;

class Vector {
public:
    Vector(int s) :elem{new double[s]}, sz{s} {
        cout << "create vector of size " << s << endl;
        for (int i = 0; i != s; ++i) {
            elem[i] = 0;
        }
    }
    Vector(initializer_list<double>);

    ~Vector() {
        cout << "destroying vector of size " << sz << endl;
        delete[] elem;
    }

    double& operator[](int i);
    int size() const { return sz; }

private:
    double* elem;
    int sz;
};

void fct(int n) {
    Vector v(n);
    cout << "... use v first time" << endl;
    {
        Vector v2(2*n);
        cout << "... use v2" << endl;
    }
    cout << "... use v second time" << endl;
}

Vector::Vector(initializer_list<double> lst)
    :elem{new double[lst.size()]}, sz{static_cast<int>(lst.size())}
{
    copy(lst.begin(), lst.end(), elem);
}

int main() {
    cout << "Vector demo starting..." << endl;
    fct(5);
    cout << "Vector demo ended" << endl;

    Vector v1 = {1, 2, 3, 4, 5};
    Vector v2 = {1.23, 3.45, 6.7, 8};
    cout << "v1 size" << v1.size() << endl;
    cout << "v2 size" << v2.size() << endl;

    return 0;
}
